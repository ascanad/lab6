/******************* 
  Austin Canady
  ascanad               
  Lab 5                           
  Lab Section: 003
  Nushrat Humaira              
*******************/ 

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  
  //Declares array of 52 structs and uses loop to initialize
  Card deck[52];
  for(int cards = 0, i = 0; i < 4; ++i) {
  	for(int j = 0; j < 13; ++j) {
  		deck[cards].suit = static_cast <Suit>(i); 
  		deck[cards].value = j + 2;
  		++cards;	
  	}
  }

  //Randomizes deck
  std::random_shuffle ( deck, deck+53, myrandom);

  //Array of 5 structs to create hand
  Card hand[5] = { deck[0], deck[1], deck[2], deck[3], deck[4] }; 

  //Sorts hand by value and suit
  std::sort(hand, hand+5, suit_order);

  //Loop to get strings and print values
  for(int i = 0; i < 5; ++i) {
  	cout<<setw(10)<<right<<get_card_name(hand[i])<<
  	" of "<<get_suit_code(hand[i])<< "\n\n";
  }

  return 0;
}



bool suit_order(const Card& lhs, const Card& rhs) {

  //Compares suit or values of left and right cards
  //Checks value if suits are the same
  if(lhs.suit < rhs.suit) {
  	return true;
  }
  else if(lhs.suit == rhs.suit) {
  	if(lhs.value < rhs.value) {
  		return true;
  	}
  	else {
  		return false;
  	}
  }
  else {
  	return false;
  }

}

//Returns code to print symbol of card suit
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//Returns the name/value of card through for loop
string get_card_name(Card& c) {
  //string variable to return
  string cardValue;

  //Loop checks value of c.value and returns correct name.
  for(int i = 2; i < 11; ++i) {
  	//Checks spades values and converts to string
	if(c.value < 11) {
		if(c.value/i == 1){
			cardValue = std::to_string(c.value);
			return cardValue;
		}
	}
	//Checks hearts values and converts to string
	else if(c.value > 14 && c.value < 23) {
		if(c.value/(2*i) == 1) {
			c.value = (c.value/(2*i));
			cardValue = std::to_string(c.value);
			return cardValue;
		}
	}	
	//Checks diamonds values and converts to string 
	else if(c.value > 26 && c.value < 35) {
		if(c.value/(3*i) == 1) {
			c.value = (c.value/(3*i));
			cardValue = std::to_string(c.value);
			return cardValue;
		}
	}
	//Checks clubs values and converts to string
	else if(c.value > 38 && c.value < 47) {
		if(c.value/(4*i) == 1) {
			c.value = (c.value/(4*i));
			cardValue = std::to_string(c.value);
			return cardValue;
		}
	}
	//Else checks for face cards and returns name
	else {
		if(c.value % 11 == 0) {
			return "Jack";
		}
		else if(c.value % 12 == 0) {
			return "Queen";
		}
		else if(c.value % 13 == 0) {
			return "King";
		}
		else {
			return "Ace";
		}
	}
  }
  return "";
}

